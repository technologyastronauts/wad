# -*- encoding: utf-8 -*-

# Yes, we don't use generators and hoes for this. This is simple enough. 

Gem::Specification.new do |s|
  s.name = 'wad'
  s.version = '0.2.1'

  s.authors = ['Kaspar Schiess', 'Florian Hanke']
  s.email = [
    'kaspar.schiess@technologyastronauts.ch', 
    'florian.hanke@technologyastronauts.ch']
  s.extra_rdoc_files = ['README']

  # Generate this manifest using 'git ls-tree -r --name-only master'
  s.files = File.readlines('manifest.txt')
    .map { |l| l.chomp } -
      %w(.gitignore manifest.txt)
  s.homepage = 'https://bitbucket.org/technologyastronauts/wad'
  s.rdoc_options = ['--main', 'README']
  s.require_paths = ['lib']

  s.license = 'MIT'

  s.summary = 
  'After rubygems and bundler, let us take you full circle and go back to a 
lib directory that contains all your libraries. Except now, it\'s per project. '

  s.description = <<-EOD
    Since we're all following very strict standards with regards to how our gems 
    are constructed, we might as well pack all those gems back into a directory 
    and use that directory in our load path. 

    Once you do that, you'll discover that loading from all these paths and doing 
    dependency resolution cost on every ruby invocation. On our machines, using 
    wad saves us >500ms every time, on every call. 

    Wad helps you with getting there: It vendors your Gemfile below `vendor/bundle`, 
    then copies relevant source code to `vendor/lib`. All in one simple call. 
  EOD

  s.executables << 'wad'

  s.add_runtime_dependency 'clamp', '~> 0.6'
  s.add_runtime_dependency 'bundler', '> 0'
  s.add_runtime_dependency 'mixlib-shellout', '~> 2.0'
end
