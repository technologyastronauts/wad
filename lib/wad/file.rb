
module Wad

  # A file that is part of a gem. 
  #
  class File
    # Full file path
    attr_reader :path

    # Relative path this file should have after the copy
    attr_reader :to

    # Gem this belongs to
    attr_reader :ghem

    def initialize ghem, path, to
      @ghem = ghem
      @path = path
      @to = to
    end

    def copy target
      target.copy(self)
    end

    def gem_name
      ghem.name
    end

    def directory?
      path.directory?
    end
  end
end
