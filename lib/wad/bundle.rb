
require 'mixlib/shellout'

module Wad
  # Interaction with bundler THROUGH THE COMMAND LINE SINCE THE CODE IS REALLY
  # COMPLICATED.
  #
  class Bundle
    class LiveDelegator
      def initialize &block
        @block = block
      end

      def << buffer
        buffer.lines.each do |line|
          @block.call(line.chomp)          
        end
      end
    end

    def install &block
      status = bundle(:install, live_stream: LiveDelegator.new(&block))
      ! status.error?
    end

    def list *a
      cmd = bundle(:list, *a, raise_on_error: true)
      cmd.stdout
    end

  private
    def bundle *a, **opts
      raise_on_error = opts.delete(:raise_on_error)

      timeout = 60*60 # seconds
      a = a.map { |e| e.to_s }
      a.unshift 'bundle'

      runner = Mixlib::ShellOut.new(
        *a, 
        opts.merge(timeout: timeout))

      runner.run_command

      if raise_on_error
        runner.error!
      end

      runner
    end
  end
end