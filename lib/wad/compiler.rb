

require 'fileutils'

require 'rubygems/specification'

require 'wad/gem'
require 'wad/target'
require 'wad/reporter'
require 'wad/bundle'

module Wad
  class Compiler

    attr_reader :root
    attr_reader :reporter
    attr_reader :bundle

    # Constructs a new compiler which will do its work in root/vendor, copying
    # vendor/bundle to vendor/lib.
    # 
    # @param root [Pathname] root of the project
    #
    def initialize root
      @root = root
      @reporter = Reporter.new
      @bundle = Bundle.new
    end

    def compile
      target = Target.new(root, 'vendor/lib', reporter)

      empty(target)
      bundle_install or raise "Please fix bundler failures first."
      copy_files(target)
    end

    def empty target
      reporter.report_emptying(target)
      # Danger.
      FileUtils.rm_rf(target.path)
    end

    def bundle_install
      status = false # declare local var

      reporter.report_bundle_install do
        status = bundle.install do |line|
          reporter.report_bundle_install_line(line)
        end
      end

      status
    end

    def copy_files target
      reporter.report_copy_phase(target)

      # my editor messes up highlighting upon encountering 'gem'... so ghem it is. 
      each_gem do |ghem| 
        reporter.report_copy_for(ghem) do
          ghem.each_lib_file do |file|
            # We can't 'copy' directories
            next if file.directory?

            file.copy(target)
          end
        end
      end
    end

  private

    def each_gem
      bundle_list do |path, spec|
        yield Gem.new(path, spec)
      end
    end

    # @yield path_to_gem, gem_spec
    #
    def bundle_list
      bundle.list('--paths').lines.each do |line|
        path = Pathname.new(line.chomp)

        gemspec_file = Pathname.glob(path.join('*.gemspec')).first
        spec = begin
          ::Gem::Specification.load(gemspec_file.to_s)
        rescue Exception => e
          nil # Ignore exceptions in Bundler, including SystemExit
        end

        yield path, spec
      end
    end
  end
end