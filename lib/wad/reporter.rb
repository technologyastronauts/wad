
require 'wad/vendor/highlight'

String.highlighter = Text::ANSIHighlighter.new

module Wad

  class Gig
    def initialize text
      @n = 0
      @text = text

      put
    end

    def put
      a = %w(| / - \\)

      ab(@text, a[@n % a.size])

      inc
    end

    def done status
      ab(@text, status)
      puts
    end

    def ab a, b
      printf("\r%s [%s]", a, b)
    end

    def inc
      @n += 1
    end
  end

  # Reports on progress. 
  # 
  class Reporter
    attr_reader :gig

    def report_emptying target
      puts "Cleaning #{target}."
    end

    # @param target [Target] copy target
    #
    def report_copy_phase target
      # printf "-> %s\n", target.to_s.green
    end

    # @param ghem [Gem] gem that copy phase is executed for
    # @yield
    #
    def report_copy_for ghem
      @gig = Gig.new(ghem.name.yellow)
      @count = 0

      yield
      
      @gig.done 'done'.green + " (#{@count} files copied)"
    end

    # @param gem_file [File] gem file that is being copied
    # @param path [Pathname] target path
    #
    def report_copy_file gem_file, path
      @count += 1
      @gig.put
    end

    # @yield
    # 
    def report_bundle_install
      printf("Performing a `%s`...\n", "bundle install".yellow)
      yield
      printf(" > %s\n", '...looks good!'.green)
    end

    def report_bundle_install_line line
      puts " > #{line}"
    end
  end
end