
require 'wad/file'

module Wad

  # Our internal representation of a gem that we copy files from. 
  #
  class Gem
    # Base directory of the gem version
    attr_reader :base
    # Name of the gem, currently including the version
    attr_reader :name
    # @return [Gem::Specification] loaded gemspec
    attr_reader :spec

    def initialize path, spec
      @base = path
      @spec = spec
      @name = path.split.last.to_s
    end

    def each_lib_file
      each_require_path do |subdir|
        subdir_path = base.join(subdir)

        wildcard = base.join("#{subdir}/**/*")
        Pathname.glob(wildcard).each do |path|
          yield File.new(self, path, path.relative_path_from(subdir_path))
        end
      end
    end

    def each_require_path &block
      if spec
        # Filter the require paths to not include: 
        #   a) '.', because that would mess up the toplevel dir in vendor/lib
        #   b) '../...', because those paths mostly point to bundles, 
        #     which also exist in lib!
        allowed_require_paths = Array(spec.require_paths)
          .reject { |rqp| rqp.start_with?('.') }

        allowed_require_paths.each(&block)
      else
        # If gemspec wasn't in the gem directory, let's just cheat.
        yield 'lib'
      end
    end
  end
end