
module Wad

  # A target directory that will gem files through #copy. Also keeps track of 
  # what files have already been written. 
  #
  class Target
    attr_reader :path
    attr_reader :postfix

    attr_reader :existing
    attr_reader :old_source
    
    attr_reader :reporter

    def initialize path, postfix, reporter
      @postfix = postfix
      @path = path.join(postfix)
      @existing = {}
      @old_source = {}
      @reporter = reporter

      FileUtils.mkdir_p(path)
    end

    def to_s
      postfix
    end

    def copy gem_file
      key = gem_file.to.to_s
      gem_name = gem_file.gem_name

      # Try to detect multiple copies of one and the same gem file.
      if existing.has_key?(key) && existing[key] != gem_name
        name = existing[key]
        old_path = old_source[key]

        raise "Cannot copy #{gem_file.path} from #{gem_name}, already exists from #{name} (@#{old_path.to_s})."
      else
        existing[key] = gem_name
        old_source[key] = gem_file.path

        reporter.report_copy_file(gem_file, path)

        dest = path.join(gem_file.to)
        FileUtils.mkdir_p(dest.dirname)
        FileUtils.cp(gem_file.path, dest) unless dest.file?
      end
    end
  end
end