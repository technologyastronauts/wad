#!/usr/bin/ruby -w

# Our vendored copy of text-highlight (https://rubygems.org/gems/text-highlight)
# version 1.0.2 by Jeff Pace.

module Text

  # Highlights text using either ANSI terminal codes, or HTML.

  class Highlighter

    VERSION = "1.0.2"

    ATTRIBUTES = %w{
      none
      reset
      bold
      underscore
      underline
      blink
      reverse
      concealed
      black
      red
      green
      yellow
      blue
      magenta
      cyan
      white
      on_black
      on_red
      on_green
      on_yellow
      on_blue
      on_magenta
      on_cyan
      on_white
    }

    NONE = Object.new
    HTML = Object.new
    ANSI = Object.new

    # Colorizes the given object. If a block is passed, its return value is used
    # and the stream is reset. If a String is provided as the object, it is
    # colorized and the stream is reset. Otherwise, only the code for the given
    # color name is returned.
    
    def color(colorname, obj = self, &blk)
      #                        ^^^^ this is the Module self
      result = name_to_code(colorname)
      if blk
        result << blk.call
        result << name_to_code("reset")
      elsif obj.kind_of?(String)
        result << obj
        result << name_to_code("reset")
      end
      result
    end

    ATTRIBUTES.each do |attr|
      code = <<-EODEF
      def #{attr}(&blk)
        color("#{attr}", &blk)
      end

      EODEF

      eval code
    end

    # returns the code for the given color string, which is in the format:
    # foreground* [on background]?
    # 
    # Note that the foreground and background sections can have modifiers
    # (attributes).
    # 
    # Examples:
    #     black
    #     blue on white
    #     bold green on yellow
    #     underscore bold magenta on cyan
    #     underscore red on cyan

    def code(str)
      fg, bg = str.split(/\s*\bon_?\s*/)
      (fg ? foreground(fg) : "") + (bg ? background(bg) : "")
    end

    # Returns the code for the given background color(s).
    def background(bgcolor)
      name_to_code("on_" + bgcolor)
    end

    # Returns the code for the given foreground color(s).
    def foreground(fgcolor)
      fgcolor.split(/\s+/).collect { |fg| name_to_code(fg) }.join("")
    end

  end


  # Highlights using HTML. Fonts are highlighted using <span> tags, not <font>.
  # Also note that reverse is translated to white on black.
  # According to http://www.w3.org/TR/REC-CSS2/syndata.html#value-def-color,
  # valid color keywords are: aqua, black, blue, fuchsia, gray, green, lime,
  # maroon, navy, olive, purple, red, silver, teal, white, and yellow.
  # Thus, no magenta or cyan.

  class HTMLHighlighter < Highlighter

    def initialize
      # we need to know what we're resetting from (bold, font, underlined ...)
      @stack = []
    end

    # Returns the start tag for the given name.
    
    def start_style(name)
      case name
      when "reverse"
        "<span style=\"color: white; background-color: black\">"
      when /on_(\w+)/
        "<span style=\"background-color: #{$1}\">"
      else
        "<span style=\"color: #{name}\">"
      end
    end

    # Returns the end tag ("</span>").

    def end_style
      "</span>"
    end

    def color_value(cname)
      case cname
      when "cyan"
        "#00FFFF"
      when "magenta"
        "#FF00FF"
      else
        cname
      end
    end

    # Returns the code for the given name.

    def name_to_code(name)
      @stack << name

      case name
      when "none", "reset"
        @stack.pop
        str = ""
        if @stack.length > 0
          begin
            prev = @stack.pop
            case prev
            when "bold"
              str << "</b>"
            when "underscore", "underline"
              str << "</u>"
            when "blink"
              str << "</blink>"
            when "concealed"
              str << " -->"
            else
              str << end_style
            end
          end while @stack.length > 0
        end
        str
      when "bold"
        "<b>"
      when "underscore", "underline"
        "<u>"
      when "blink"
        "<blink>"
      when "concealed"
        "<!-- "
      else
        start_style(name)
      end
    end

  end


  # Highlights using ANSI escape sequences.

  class ANSIHighlighter < Highlighter

    @@ATTRIBUTES = Hash[
      'none'       => '0', 
      'reset'      => '0',
      'bold'       => '1',
      'underscore' => '4',
      'underline'  => '4',
      'blink'      => '5',
      'reverse'    => '7',
      'concealed'  => '8',
      'black'      => '30',
      'red'        => '31',
      'green'      => '32',
      'yellow'     => '33',
      'blue'       => '34',
      'magenta'    => '35',
      'cyan'       => '36',
      'white'      => '37',
      'on_black'   => '40',
      'on_red'     => '41',
      'on_green'   => '42',
      'on_yellow'  => '43',
      'on_blue'    => '44',
      'on_magenta' => '45',
      'on_cyan'    => '46',
      'on_white'   => '47',
    ]

    # Returns the escape sequence for the given name.

    def name_to_code(nm)
      "\e[#{@@ATTRIBUTES[nm]}m"
    end

  end

  
  # Does no highlighting.

  class NonHighlighter < Highlighter

    # Since the NonHighlighter does no highlighting, and thus its name, this
    # returns an empty string.

    def name_to_code(colorname)
      ""
    end

  end


  # An object that can be highlighted. This is used by the String class.

  module Highlightable

    # The highlighter for the class in which this module is included.

    @@highlighter = NonHighlighter.new
    
    Text::Highlighter::ATTRIBUTES.each do |attr|
      code = <<-EODEF
      def #{attr}(&blk)
        @@highlighter.color("#{attr}", self, &blk)
      end

      EODEF

      eval code
    end

    alias negative reverse

    # Sets the highlighter for this class. This can be either by type or by
    # String.

    def highlighter=(hl)
      $VERBOSE = false
      @@highlighter = case hl
                      when Text::Highlighter
                        hl
                      when Text::Highlighter::NONE, "NONE", nil
                        Text::NonHighlighter.new  unless @@highlighter.kind_of?(Text::NonHighlighter)
                      when Text::Highlighter::HTML, "HTML"
                        Text::HTMLHighlighter.new unless @@highlighter.kind_of?(Text::HTMLHighlighter)
                      when Text::Highlighter::ANSI, "ANSI"
                        Text::ANSIHighlighter.new unless @@highlighter.kind_of?(Text::ANSIHighlighter)
                      else
                        Text::NonHighlighter.new
                      end
    end

  end

  $HAVE_TEXT_HIGHLIGHT = true

end


# String is extended to support highlighting.

class String
  include Text::Highlightable
  extend Text::Highlightable
end
